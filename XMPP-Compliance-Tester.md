### XMPP Compliance Tester
```
jar_file=ComplianceTester-0.2.3.jar
dl_url=https://gultsch.de/files/$jar_file
domain=im.example.net
user=johndoe
pass=123456qwerty

docker run --rm -it --name=xmpptest openjdk:alpine \
/bin/sh -c "wget $dl_url ; java -jar $jar_file $user@$domain $pass" \
| tee $domain.txt
```