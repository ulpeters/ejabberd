Monitor if file permissions changed and change them back

```
docker run -it --rm -d \
-v /opt/docker/proxy/data/certs/im.s-up.net/:/home/ejabberd/ssl/ \
--name ejabberd-perms alpine \
sh -c 'apk add --no-cache inotify-tools
while true; do inotifywait /home/ejabberd/ssl/key.pem  --event attrib \
&& chmod -R o+r,o+X /home/ejabberd/ssl/; done'
```